#!/usr/bin/env python3
#
# Copyright 2024-2024 Canonical Ltd.
# Authors:
# - Hector Cao <hector.cao@canonical.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 3, as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranties of MERCHANTABILITY,
# SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#

import unittest
import subprocess

from UEFI.Qemu import QemuEfiMachine, QemuEfiFlashSize
from UEFI import Qemu

DPKG_ARCH = subprocess.check_output(
    ['dpkg', '--print-architecture']
).decode().rstrip()

class SuspendTest(unittest.TestCase):
    debug = True

    @unittest.skipUnless(DPKG_ARCH == 'amd64', "amd64-only")
    def test_suspend(self):
        """
        Test the S3 suspend feature with OVMF using QEMU
        This test will:
        - run QEMU with the host kernel and generated initrd
          (the initrd init binary will put the VM in S3 state and poweroff at resume)
        - using QEMU monitor interface:
          - check that the VM is in suspended state
          - send wakeup command to VM
        - wait for QEMU to exit properly (at poweroff of the VM)
        """
        initrd_init_cmd = '''
                          echo mem > /sys/power/state
        '''
        subprocess.check_call('python3 debian/tests/make-tiny-image.py --run="%s"'
                              % (initrd_init_cmd), shell=True)
        q = Qemu.QemuCommand(
              QemuEfiMachine.OVMF_Q35,
              flash_size=QemuEfiFlashSize.SIZE_4MB,
            )
        q.add_kernel(None)
        q.add_initrd('./tiny-initrd.img')
        q.add_monitor()
        pro = subprocess.Popen(q.command, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        try:
            m = Qemu.QemuMonitor(q)
            m.wait_for_state('suspended')
            m.wakeup()
            pro.wait(timeout=30)
        except Exception as e:
            self.fail('Error : %s' % (e))

if __name__ == '__main__':
    unittest.main(verbosity=2)
